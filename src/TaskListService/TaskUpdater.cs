﻿using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListDAL.Interface;
using TaskListService.Dtos;
using TaskListService.Interfaces;

namespace TaskListService
{
    public class TaskUpdater : ITaskUpdater
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper<TaskItem, TaskItemDto> mapper;

        public TaskUpdater(IUnitOfWork unitOfWork, IMapper<TaskItem, TaskItemDto> mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> AddAsync(TaskItemDto taskItemDto)
        {
            var repository = unitOfWork.GetRepository<TaskItem>();
            var entity = mapper.DtoToEntity(taskItemDto);
            entity = entity.Id == 0 
                ? await repository.AddAsync(entity) 
                : repository.Update(entity);
            return await unitOfWork.SaveAsync() > 0;
        }

        public async Task<bool> DeleteAsync(long id)
        {
            var repository = unitOfWork.GetRepository<TaskItem>();
            var entity = await repository.GetOneAsync(id);
            if (entity != null)
            {
                repository.Delete(entity);
                return await unitOfWork.SaveAsync() > 0;
            }
            return false;
        }
    }
}