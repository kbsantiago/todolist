﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListDAL.Interface;
using TaskListService.Dtos;
using TaskListService.Interfaces;

namespace TaskListService
{
    public class TaskProvider : ITaskProvider
    {
        private readonly IUnitOfWork unitOfWork;

        public TaskProvider(IUnitOfWork unitOfWork) =>
            this.unitOfWork = unitOfWork;

        public async Task<IEnumerable<TaskItemDto>> GetTasksAsync()
        {
            var repo = unitOfWork.GetRepository<TaskItem>();

            var list = await repo.GetAllAsync();

            return list.Select(i => new TaskItemDto
            {
                Id = i.Id,
                Title = i.Title
            });
        }
    }
}
