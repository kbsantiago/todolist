﻿using System;
using System.Linq.Expressions;
using TaskListDAL.Entities;
using TaskListDAL.Interface;

namespace TaskListService.Specification
{
    public class TasksByTitleSpecification : IQuerySpecification<TaskItem>
    {
        public Expression<Func<TaskItem, bool>> Criteria { get; }

        public TasksByTitleSpecification(Expression<Func<TaskItem, bool>> criteria) =>
            Criteria = criteria;
    }
}
