﻿using System;
using TaskListDAL.Entities;

namespace TaskListService.Dtos
{
    public class TaskItemDto
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public Status Status { get; set; }
    }
}
