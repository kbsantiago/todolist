﻿using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListService.Dtos;
using TaskListService.Interfaces;

namespace TaskListService.Mapper
{
    public class TaskItemMapper : IMapper<TaskItem, TaskItemDto>
    {
         public TaskItem DtoToEntity(TaskItemDto dto)
        {
            return new TaskItem
            {
                Id = dto.Id,
                Title = dto.Title,
                //Description = dto.Description,
                Status = dto.Status,
                //CreationDate = dto.CreationDate,
                //UpdateDate = dto.UpdateDate,
                //ConclusionDate = dto.ConclusionDate
            };
        }

        public TaskItemDto EntityToDto(TaskItem t)
        {
            return new TaskItemDto
            {
                Id = t.Id,
                Title = t.Title,
                //Description = t.Description,
                Status = t.Status,
                //CreationDate = t.CreationDate,
                //UpdateDate = t.UpdateDate,
                //ConclusionDate = t.ConclusionDate
            };
        }
    }
}
