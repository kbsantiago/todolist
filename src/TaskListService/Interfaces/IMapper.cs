﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TaskListService.Interfaces
{
    public interface IMapper<TEntity, TDto> where TEntity : class where TDto : class
    {
        TDto EntityToDto(TEntity t);
        TEntity DtoToEntity(TDto t);

    }
}
