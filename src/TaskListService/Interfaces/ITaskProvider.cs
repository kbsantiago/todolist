﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskListService.Dtos;

namespace TaskListService.Interfaces
{
    public interface ITaskProvider
    {
        Task<IEnumerable<TaskItemDto>> GetTasksAsync();
    }
}
