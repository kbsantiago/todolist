﻿using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListService.Dtos;

namespace TaskListService.Interfaces
{
    public interface ITaskUpdater
    {
        Task<bool> AddAsync(TaskItemDto taskItemDto);
        Task<bool> DeleteAsync(long id);
    }
}