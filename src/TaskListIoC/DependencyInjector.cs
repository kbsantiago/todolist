﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TaskListDAL;
using TaskListDAL.Entities;
using TaskListDAL.Interface;
using TaskListService;
using TaskListService.Dtos;
using TaskListService.Interfaces;
using TaskListService.Mapper;

namespace TaskListIoC
{
    public static class DependencyInjector
    {
        public static void Register(IServiceCollection services)
        {
            services.TryAddScoped<ITaskProvider, TaskProvider>();
            services.TryAddScoped<ITaskUpdater, TaskUpdater>();
            services.TryAddScoped<IUnitOfWork, UnitOfWork>();
            services.TryAddScoped<IMapper<TaskItem, TaskItemDto>, TaskItemMapper>();            
        }
    }
}
