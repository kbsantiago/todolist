﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListDAL.Interface;

namespace TaskListDAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext context;
        private readonly Dictionary<Type, object> repositories;

        public UnitOfWork(DatabaseContext context)
        {
            this.context = context;
            repositories = new Dictionary<Type, object>();
        }
            

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            var type = typeof(TEntity);
            if (!repositories.ContainsKey(type))
            {
                repositories.Add(typeof(TEntity),
                    new Repository<TEntity>(context));
            }
            return repositories[type] as IRepository<TEntity>;
        }

        public Task<int> SaveAsync() =>
            context.SaveChangesAsync();
    }
}
