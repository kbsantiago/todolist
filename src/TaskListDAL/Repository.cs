﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskListDAL.Entities;
using TaskListDAL.Interface;

namespace TaskListDAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext context;

        public Repository(DbContext context) : base() =>
            this.context = context;

        public Task<TEntity> GetOneAsync(long id) =>
            context.Set<TEntity>().FirstOrDefaultAsync(e => e.Id == id);

        public Task<List<TEntity>> GetAllAsync() =>
            context.Set<TEntity>().ToListAsync();

        public Task<List<TEntity>> GetAsync(IQuerySpecification<TEntity> spec)
        {
            return context.Set<TEntity>()
                .Where(spec.Criteria)
                .ToListAsync();
        }

        public async Task<TEntity> AddAsync(TEntity entity) 
        {
            return (await context.AddAsync<TEntity>(entity)).Entity;
        }

        public TEntity Update(TEntity entity)
        {
            return context.Update(entity).Entity;
        }

        public void Delete(TEntity entity)
        {
            context.Remove(entity);
        }
    }
}
