﻿using System;
using System.Linq.Expressions;

namespace TaskListDAL.Interface
{
    public interface IQuerySpecification<T>
    {
        Expression<Func<T, bool>> Criteria { get; }
    }
}
