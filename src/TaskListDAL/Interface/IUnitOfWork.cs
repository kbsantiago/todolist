﻿using System.Threading.Tasks;
using TaskListDAL.Entities;

namespace TaskListDAL.Interface
{
    public interface IUnitOfWork
    {
        IRepository<T> GetRepository<T>() where T : BaseEntity;
        Task<int> SaveAsync();
    }
}
