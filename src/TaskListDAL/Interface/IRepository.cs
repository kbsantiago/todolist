﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaskListDAL.Entities;

namespace TaskListDAL.Interface
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<TEntity> GetOneAsync(long id);
        Task<List<TEntity>> GetAllAsync();
        Task<List<TEntity>> GetAsync(IQuerySpecification<TEntity> spec);
        Task<TEntity> AddAsync(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
