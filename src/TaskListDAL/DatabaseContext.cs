﻿using Microsoft.EntityFrameworkCore;
using TaskListDAL.Entities;

namespace TaskListDAL
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<TaskItem> Items { get; set; }
    }
}
