﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskListDAL.Entities
{
    public enum Status
    {
        EmAndamento = 0,
        Concluido = 1
    }
}
