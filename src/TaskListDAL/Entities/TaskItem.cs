﻿using System;

namespace TaskListDAL.Entities
{
    public class TaskItem : BaseEntity
    {
        public string Title { get; set; }
        public Status Status { get; set; }
    }
}
