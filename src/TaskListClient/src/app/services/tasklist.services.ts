import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators';

@Injectable()
export class WebApiService {

  private readonly endpoint = "https://localhost:44373/api/Task";
  private 

  constructor(private http: HttpClient) {
  }

  getTasks(): Observable<any> {
    return this.http.get(this.endpoint).pipe(map(this.extractData));
  }

  addTask(task): Observable<any> {
    return this.http.post(this.endpoint, task).pipe(map(this.extractData));
  }

  deleteTask(id): Observable<any> {
    return this.http.delete(this.endpoint, id).pipe();
  }

  updateTask(task): Observable<any> {
    return this.http.put(this.endpoint, task).pipe(map(this.extractData));
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
