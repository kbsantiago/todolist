import { Component } from '@angular/core';
import { WebApiService } from '../services/tasklist.services';
import { ITask } from '../models/task';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  tasks = [];
  task: ITask = {id: 0, title: ""};
  
  constructor(private webapi: WebApiService) {
    this.getTasks();
  }

  getTasks() {
    this.tasks = [];
    this.webapi.getTasks().subscribe((data: any) => {
      console.log(data);
      this.tasks = data;
    });
  }

  addTask() {
    this.webapi.addTask(this.task).subscribe((data:any) => {
      this.getTasks();
      this.task.title = "";
    });
  }

  removeTask(id: number) {
    this.webapi.deleteTask(id).subscribe((data:any) => {
      this.getTasks();
      
    });
  }

  updateTask() {
    this.webapi.updateTask(this.task).subscribe((data:any) => {
      this.getTasks();
    });
  }
  
  
}

