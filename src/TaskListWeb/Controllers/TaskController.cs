﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TaskListService.Dtos;
using TaskListService.Interfaces;

namespace TaskListWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskProvider taskProvider;
        private readonly ITaskUpdater taskUpdater;

        public TaskController(ITaskProvider taskProvider, ITaskUpdater taskUpdater)
        {
            this.taskProvider = taskProvider;
            this.taskUpdater = taskUpdater;
        }
            

        [HttpGet]
        public async Task<IActionResult> Get() =>
            Ok(await taskProvider.GetTasksAsync());

        [HttpPost]
        public async Task<IActionResult> Post(TaskItemDto taskItemDto) =>
            Ok(await taskUpdater.AddAsync(taskItemDto));

        [HttpPut]
        public async Task<IActionResult> Put(TaskItemDto taskItemDto) =>
            Ok(await taskUpdater.AddAsync(taskItemDto));

        [HttpDelete]
        public async Task<IActionResult> Delete(long id) =>
            Ok(await taskUpdater.DeleteAsync(id));
    }
}
